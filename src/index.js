'use-strict'

import 'formdata-polyfill';

window.addEventListener('load', function () {
    const form = document.getElementById('searchform');
    const blogResults = this.document.getElementById('blog-results');
    const favoritesContainer = this.document.getElementById('favorites-container');
  
    form.addEventListener("submit", function (event) {
      event.preventDefault();  
      getBlog();
    });

    blogResults.addEventListener('click', function (event) {
        if (event.target.classList.contains('add-favorite')) {
            addFavorite(event.target);
        }
    });

    favoritesContainer.addEventListener('click', function (event) {
        if (event.target.classList.contains('add-favorite')) {
            removeFavorite(event.target);
        }
    });

    const getBlog = async function () {
        const formdata = new FormData(form);
        const blogname = formdata.get('blogname');
        const blogtag = formdata.get('blogtag');

        if (!blogname && !blogtag) {
            window.alert('Form is empty');
            return;
        }

        const results = await fetch(`/search?name=${blogname}&tag=${blogtag}`);
        const resultsJSON = await results.json();

        if (!resultsJSON.length) {
            window.alert('No Results Found');
            return;
        }

        /* CLEAR OUT FAVORITES IF NEW SEARCH */
        let f = favoritesContainer.getElementsByTagName('article') || [];
        if (f.length) {
            for (let i=0; f.length; i++) {
                f[0].parentNode.removeChild(f[0]);
            }
        }

        let template = '';

        resultsJSON.map((x, index) => {
            template +=
            `
                <article class='card' data-entry=${index} >
                    <header>
                        <h4 class='m--1 g--10'>
                            <div class='right'>
                                <i class='tooltip far fa-heart add-favorite' data-text='Favorite' data-entry=${index}></i>
                            </div>
                            <div>
                                ${index+1}. <a href= ${x.post_url} target=blank> ${x.blog_name} <span class='fas fa-external-link-alt'></span> </a>
                            </div>
                        </h4>
                    </header>
                    ${ x.asking_name ? '<a href=' + x.asking_url + '>' + x.asking_name + '</a> asked: ' : '' }
                    ${ (!x.caption && x.summary) ? '<p>' + x.summary + '</p>' : '' }
                    ${ x.asking_name ? x.answer : '' }
                    <p>
                        ${x.body || ''}
                    </p>
                    ${ (x.photos && x.photos.length > 0) ? '<figure><img src= ' + x.photos[0].original_size.url + '></figure>' : '' }
                    ${ x.thumbnail_url ? '<figure><img src= ' + x.thumbnail_url + '></figure>' : '' }
                    ${ x.caption ? '<p>' + x.caption + '</p>' : '' }
                    <p>
                        ${x.description || ''}
                    <p>
                        ${x.tags.map((tagname)=>`#${tagname} `)}
                    </p>
                </article>
            `
        });

        blogResults.innerHTML = template;
    };

    const addFavorite = (element) => {
        const entry = element.dataset.entry;
        const blogEntry = document.querySelector('.card[data-entry="' + entry + '"]')
        blogEntry.getElementsByClassName('fa-heart')[0].dataset.text = 'Unfavorite';
        favoritesContainer.appendChild(blogEntry);
    };

    const removeFavorite = (element) => {
        const entry = element.dataset.entry;
        const blogEntry = document.querySelector('.card[data-entry="' + entry + '"]')
        blogEntry.getElementsByClassName('fa-heart')[0].dataset.text = 'Favorite';
        blogResults.appendChild(blogEntry);
    }

});
