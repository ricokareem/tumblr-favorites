const chai = require('chai');
const expect = chai.expect;

const router = require('../../../routes/index');

describe('Integration:getBlog', () => {
    describe('if only a tag is given', () =>{
        it('returns an Array', async () => {
            return router._test.getBlog('','california').then(data => expect(data.blog.response).to.be.an('array'));
        });
        it('Array includes posts', async () => {
            return router._test.getBlog('','california').then(data => expect(data.blog.response[0]).to.have.property('blog_name'));
        });
    });
    describe('if a blog name is given', () => {
        it('returns an Object', async () => {
            return router._test.getBlog('peacecorp','california').then(data => expect(data.blog.response).to.be.an('object'));
        });
        it('returns an Object', async () => {
            return router._test.getBlog('peacecorp','california').then(data => expect(data.blog.response).to.have.property('posts'));
        });
    });
});