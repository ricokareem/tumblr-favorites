/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_formdata_polyfill__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_formdata_polyfill___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_formdata_polyfill__);
'use-strict'



window.addEventListener('load', function () {
    const form = document.getElementById('searchform');
    const blogResults = this.document.getElementById('blog-results');
    const favoritesContainer = this.document.getElementById('favorites-container');
  
    form.addEventListener("submit", function (event) {
      event.preventDefault();  
      getBlog();
    });

    blogResults.addEventListener('click', function (event) {
        if (event.target.classList.contains('add-favorite')) {
            addFavorite(event.target);
        }
    });

    favoritesContainer.addEventListener('click', function (event) {
        if (event.target.classList.contains('add-favorite')) {
            removeFavorite(event.target);
        }
    });

    const getBlog = async function () {
        const formdata = new FormData(form);
        const blogname = formdata.get('blogname');
        const blogtag = formdata.get('blogtag');

        if (!blogname && !blogtag) {
            window.alert('Form is empty');
            return;
        }

        const results = await fetch(`/search?name=${blogname}&tag=${blogtag}`);
        const resultsJSON = await results.json();

        if (!resultsJSON.length) {
            window.alert('No Results Found');
            return;
        }

        /* CLEAR OUT FAVORITES IF NEW SEARCH */
        let f = favoritesContainer.getElementsByTagName('article') || [];
        if (f.length) {
            for (let i=0; f.length; i++) {
                f[0].parentNode.removeChild(f[0]);
            }
        }

        let template = '';

        resultsJSON.map((x, index) => {
            template +=
            `
                <article class='card' data-entry=${index} >
                    <header>
                        <h4 class='m--1 g--10'>
                            <div class='right'>
                                <i class='tooltip far fa-heart add-favorite' data-text='Favorite' data-entry=${index}></i>
                            </div>
                            <div>
                                ${index+1}. <a href= ${x.post_url} target=blank> ${x.blog_name} <span class='fas fa-external-link-alt'></span> </a>
                            </div>
                        </h4>
                    </header>
                    ${ x.asking_name ? '<a href=' + x.asking_url + '>' + x.asking_name + '</a> asked: ' : '' }
                    ${ (!x.caption && x.summary) ? '<p>' + x.summary + '</p>' : '' }
                    ${ x.asking_name ? x.answer : '' }
                    <p>
                        ${x.body || ''}
                    </p>
                    ${ (x.photos && x.photos.length > 0) ? '<figure><img src= ' + x.photos[0].original_size.url + '></figure>' : '' }
                    ${ x.thumbnail_url ? '<figure><img src= ' + x.thumbnail_url + '></figure>' : '' }
                    ${ x.caption ? '<p>' + x.caption + '</p>' : '' }
                    <p>
                        ${x.description || ''}
                    <p>
                        ${x.tags.map((tagname)=>`#${tagname} `)}
                    </p>
                </article>
            `
        });

        blogResults.innerHTML = template;
    };

    const addFavorite = (element) => {
        const entry = element.dataset.entry;
        const blogEntry = document.querySelector('.card[data-entry="' + entry + '"]')
        blogEntry.getElementsByClassName('fa-heart')[0].dataset.text = 'Unfavorite';
        favoritesContainer.appendChild(blogEntry);
    };

    const removeFavorite = (element) => {
        const entry = element.dataset.entry;
        const blogEntry = document.querySelector('.card[data-entry="' + entry + '"]')
        blogEntry.getElementsByClassName('fa-heart')[0].dataset.text = 'Favorite';
        blogResults.appendChild(blogEntry);
    }

});


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var g,k="function"==typeof Object.defineProperties?Object.defineProperty:function(b,a,d){b!=Array.prototype&&b!=Object.prototype&&(b[a]=d.value)},l="undefined"!=typeof window&&window===this?this:"undefined"!=typeof global&&null!=global?global:this;function n(){n=function(){};l.Symbol||(l.Symbol=p)}var p=function(){var b=0;return function(a){return"jscomp_symbol_"+(a||"")+b++}}();
function q(){n();var b=l.Symbol.iterator;b||(b=l.Symbol.iterator=l.Symbol("iterator"));"function"!=typeof Array.prototype[b]&&k(Array.prototype,b,{configurable:!0,writable:!0,value:function(){return u(this)}});q=function(){}}function u(b){var a=0;return v(function(){return a<b.length?{done:!1,value:b[a++]}:{done:!0}})}function v(b){q();b={next:b};b[l.Symbol.iterator]=function(){return this};return b}function w(b){q();n();q();var a=b[Symbol.iterator];return a?a.call(b):u(b)}
if(!window.FormData||!window.FormData.prototype.keys){var x=function(b,a,d){if(2>arguments.length)throw new TypeError("2 arguments required, but only "+arguments.length+" present.");return a instanceof Blob?[b+"",a,void 0!==d?d+"":"string"===typeof a.name?a.name:"Blob"]:[b+"",a+""]},y=function(b){if(!arguments.length)throw new TypeError("1 argument required, but only 0 present.");return[b+""]},A=function(b){var a=w(b);b=a.next().value;a=a.next().value;b instanceof Blob&&(b=new File([b],a,{type:b.type,
lastModified:b.lastModified}));return b},B=window.FormData,C=window.XMLHttpRequest.prototype.send,D=window.Request&&window.fetch;n();var E=window.Symbol&&Symbol.toStringTag,F=new WeakMap,G=Array.from||function(b){return[].slice.call(b)};E&&(Blob.prototype[E]||(Blob.prototype[E]="Blob"),"File"in window&&!File.prototype[E]&&(File.prototype[E]="File"));try{new File([],"")}catch(b){window.File=function(a,d,c){a=new Blob(a,c);c=c&&void 0!==c.lastModified?new Date(c.lastModified):new Date;Object.defineProperties(a,
{name:{value:d},lastModifiedDate:{value:c},lastModified:{value:+c},toString:{value:function(){return"[object File]"}}});E&&Object.defineProperty(a,E,{value:"File"});return a}}var H=function(b){F.set(this,Object.create(null));if(!b)return this;b=w(G(b.elements));for(var a=b.next();!a.done;a=b.next())if(a=a.value,a.name&&!a.disabled)if("file"===a.type)for(var d=w(a.files),c=d.next();!c.done;c=d.next())this.append(a.name,c.value);else if("select-multiple"===a.type||"select-one"===a.type)for(d=w(G(a.options)),
c=d.next();!c.done;c=d.next())c=c.value,c.selected&&this.append(a.name,c.value);else"checkbox"===a.type||"radio"===a.type?a.checked&&this.append(a.name,a.value):this.append(a.name,a.value)};g=H.prototype;g.append=function(b,a,d){var c=F.get(this);c[b]||(c[b]=[]);c[b].push([a,d])};g["delete"]=function(b){delete F.get(this)[b]};g.entries=function(){function b(b,t,I){for(;;)switch(a){case 0:z=F.get(J);h=[];m=z;for(f in m)h.push(f);r=0;case 1:if(!(r<h.length)){a=3;break}f=h[r];if(f in m){a=4;break}a=
2;break;case 4:e=w(z[f]),c=e.next();case 5:if(c.done){a=7;break}d=c.value;a=8;return{value:[f,A(d)],done:!1};case 8:if(1!=b){a=9;break}a=-1;throw I;case 9:case 6:c=e.next();a=5;break;case 7:case 2:r++;a=1;break;case 3:a=-1;default:return{value:void 0,done:!0}}}var a=0,d,c,e,f,m,r,h,z,J=this,t={next:function(a){return b(0,a,void 0)},"throw":function(a){return b(1,void 0,a)},"return":function(){throw Error("Not yet implemented");}};q();t[Symbol.iterator]=function(){return this};return t};g.forEach=
function(b,a){for(var d=w(this),c=d.next();!c.done;c=d.next()){var e=w(c.value);c=e.next().value;e=e.next().value;b.call(a,e,c,this)}};g.get=function(b){var a=F.get(this);return a[b]?A(a[b][0]):null};g.getAll=function(b){return(F.get(this)[b]||[]).map(A)};g.has=function(b){return b in F.get(this)};g.keys=function(){function b(b,h,t){for(;;)switch(a){case 0:m=w(r),f=m.next();case 1:if(f.done){a=3;break}e=f.value;c=w(e);d=c.next().value;a=4;return{value:d,done:!1};case 4:if(1!=b){a=5;break}a=-1;throw t;
case 5:case 2:f=m.next();a=1;break;case 3:a=-1;default:return{value:void 0,done:!0}}}var a=0,d,c,e,f,m,r=this,h={next:function(a){return b(0,a,void 0)},"throw":function(a){return b(1,void 0,a)},"return":function(){throw Error("Not yet implemented");}};q();h[Symbol.iterator]=function(){return this};return h};g.set=function(b,a,d){F.get(this)[b]=[[a,d]]};g.values=function(){function b(b,h,t){for(;;)switch(a){case 0:m=w(r),f=m.next();case 1:if(f.done){a=3;break}e=f.value;c=w(e);c.next();d=c.next().value;
a=4;return{value:d,done:!1};case 4:if(1!=b){a=5;break}a=-1;throw t;case 5:case 2:f=m.next();a=1;break;case 3:a=-1;default:return{value:void 0,done:!0}}}var a=0,d,c,e,f,m,r=this,h={next:function(a){return b(0,a,void 0)},"throw":function(a){return b(1,void 0,a)},"return":function(){throw Error("Not yet implemented");}};q();h[Symbol.iterator]=function(){return this};return h};H.prototype._asNative=function(){for(var b=new B,a=w(this),d=a.next();!d.done;d=a.next()){var c=w(d.value);d=c.next().value;c=
c.next().value;b.append(d,c)}return b};H.prototype._blob=function(){for(var b="----formdata-polyfill-"+Math.random(),a=[],d=w(this),c=d.next();!c.done;c=d.next()){var e=w(c.value);c=e.next().value;e=e.next().value;a.push("--"+b+"\r\n");e instanceof Blob?a.push('Content-Disposition: form-data; name="'+c+'"; filename="'+e.name+'"\r\n',"Content-Type: "+(e.type||"application/octet-stream")+"\r\n\r\n",e,"\r\n"):a.push('Content-Disposition: form-data; name="'+c+'"\r\n\r\n'+e+"\r\n")}a.push("--"+b+"--");
return new Blob(a,{type:"multipart/form-data; boundary="+b})};n();q();H.prototype[Symbol.iterator]=function(){return this.entries()};H.prototype.toString=function(){return"[object FormData]"};E&&(H.prototype[E]="FormData");[["append",x],["delete",y],["get",y],["getAll",y],["has",y],["set",x]].forEach(function(b){var a=H.prototype[b[0]];H.prototype[b[0]]=function(){return a.apply(this,b[1].apply(this,G(arguments)))}});XMLHttpRequest.prototype.send=function(b){b instanceof H&&(b=b._blob(),this.setRequestHeader("Content-Type",
b.type));C.call(this,b)};if(D){var K=window.fetch;window.fetch=function(b,a){a&&a.body&&a.body instanceof H&&(a.body=a.body._blob());return K(b,a)}}window.FormData=H};

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ }),
/* 2 */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ })
/******/ ]);