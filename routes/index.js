const express = require('express');
const fetch   = require('node-fetch');

const configs = require('../config');
const router  = express.Router();

//https://api.tumblr.com/v2/blog/pitchersandpoets.tumblr.com/posts/photo?tag=new+york+yankees&api_key=hRobjDN5UcuvjA4aJMDCSsaZ2QnQ5oVbsxw9gtnYjQXCT66W8S
//api.tumblr.com/v2/blog/{blog-identifier}/posts[/type]?api_key={key}&[optional-params=]

//https://api.tumblr.com/v2/tagged?tag=new+york+yankees&api_key=hRobjDN5UcuvjA4aJMDCSsaZ2QnQ5oVbsxw9gtnYjQXCT66W8S
//api.tumblr.com/v2/tagged

const getBlog = async function (blogname, tag) {

    const url = configs.url

    if (!blogname && tag) {
        const res = await fetch(`${url}tagged?tag=${tag}&api_key=${configs.api_key}`);
        return { blog: await res.json(), found: res.status === 200 };
    }

    const res = await fetch(`${url}blog/${blogname}.tumblr.com/posts?tag=${tag}&api_key=${configs.api_key}`);
    return { blog: await res.json(), found: res.status === 200 };
}


/* GET HOME PAGE */
router.get('/', function (req, res) {
    res.render('index', {title: 'Tumblr Favorites'});
});

/* GET ASYNC CALL TO TUMBLR API*/
router.get('/search', async (req, res) => {
    try {
        const blogname = req.query && req.query.name;
        const tag = req.query && req.query.tag;
        const getResults = await getBlog(blogname, tag);

        if (!getResults.found) {
            res.status(404).end();
            return;
        }

        const {blog} = getResults;

        // HANDLE RESPONSE FROM /blog & /posts ENDPOINT (e.g.: blogname given)
        if (blog.response.posts) {
            res.send(blog.response.posts)
        }
        // HANDLE RESPONSE FROM OTHERS (e.g.: no blog name given)
        res.send(blog.response);

    } catch(e) {
        res.status(500).send(e);
    }
})

module.exports = router;
module.exports._test = { getBlog };